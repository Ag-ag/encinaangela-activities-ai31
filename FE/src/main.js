import Vue from 'vue'
import Dashboard from './components/Dashboard'
import Book from './components/Book'
import Patron from './components/Patron'
import Setting from './components/Setting'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'
import Chartkick from 'vue-chartkick'
import Charts from 'chart.js'

Vue.use(Chartkick.use(Charts))
// Install BootstrapVue
Vue.use(BootstrapVue)
// Optionally install the BootstrapVue icon components plugin
Vue.use(IconsPlugin)

Vue.config.productionTip = false


const routes = {
  '/': Dashboard,
  '/Book': Book,
  '/Patron': Patron,
  '/Setting': Setting 
};


new Vue({

  data: {
    currentRoute: window.location.pathname,
  },

  computed: {
    currentComponent() {
      return routes[this.currentRoute];
    }
  },
  render: function (h) {
    return h(this.currentComponent);
  },
}).$mount('#app')
